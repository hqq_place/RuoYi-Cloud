**1.通过官网学习**
[微服务版本若依官网入门](http://doc.ruoyi.vip/ruoyi-cloud/document/hjbs.html#%E5%90%8E%E7%AB%AF%E8%BF%90%E8%A1%8C) 

**准备工作：**
需要自己安装好nacos，并配置好jdk的信息。（注意nacos的版本要和代码里的依赖nacos版本一致！比如都是1.4.4）

一、windows安装nacos(简单点，我用的这种方式)
二、linux安装nacos
[1.nacos安装官网](https://nacos.io/zh-cn/docs/v2/quickstart/quick-start.html)   
[有道笔记]
[2.linux初次nacos启动报错jdk的问题](https://blog.csdn.net/qq_45250634/article/details/120639140) 
[有道笔记](http://note.youdao.com/noteshare?id=f705a5417d87c3c7bb1c04a960316606&sub=676F457E9D214542AC1B7A70D6F53A6E)

[启动教程](https://blog.csdn.net/weixin_39865737/article/details/121621079)

问题：
nacos启动失败）：  
1.报错：No DataSource set  
解决：修改nacos的config目录下的配置文件application.properties文件，修改mysql的信息
![截图](文档：nacos安装启动问题整理.note
链接：http://note.youdao.com/noteshare?id=d6da025a27b6a4dbd001fd2f7403ff59&sub=E20B60606A234FF3AF47DD21E4E3F739)
特殊：我本地的情况很奇怪：数据库连接根据可以正常连接mysql,但是需要连接工具如Dbeaver连一下mysql触发一下，nacos启动就不会报这个错。